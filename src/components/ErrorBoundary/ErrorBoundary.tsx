import React from 'react'

export class ErrorBoundary extends React.Component<{
    fallback: React.ReactNode
    children: React.ReactNode | React.ReactNode[]
}> {
    state = { hasError: false }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/no-unused-vars
    static getDerivedStateFromError(error: any) {
        return { hasError: true }
    }

    componentDidCatch(error: Error, errorInfo: React.ErrorInfo): void {
        console.debug('Error Boundary catch', error, errorInfo)
    }

    render() {
        const ui = this.state.hasError
            ? this.props.fallback
            : this.props.children
        console.debug('Render ErrorBoundary', this.state, ui)
        return ui
    }
}
