/**********/
/* Domain */
/**********/

export type Product = {
    id: string // '62863b689c1bcb9946a0c8ac'
    name: string // 'Bolts'
    img: string // 'https://media.gettyimages.com/photos/bolts-and-nuts-picture-id175425827?s=2048x2048'
    availableAmount: number //1000
    minOrderAmount: number // 20
    price: number // 0.2
    currency?: Currency
    unit?: OrderUnit
}

/**
 * Product Unit Prototype
 */
export type OrderUnit = {
    name: 'piece'
    namePlural: 'pieces'
    abbr: 'pc'
    abbrPlural: 'pcs'
}
export const ORDER_UNITS: Record<'piece', OrderUnit> = {
    piece: {
        name: 'piece',
        namePlural: 'pieces',
        abbr: 'pc',
        abbrPlural: 'pcs'
    }
} as const

/**
 * Currency Prototype
 */
export type Currency = {
    name: 'US Dollar'
    namePlural: 'US Dollars'
    code: 'USD'
    symbol: '$'
    locale: 'en-US'
}

export const CURRENCIES: Record<'us_dollar', Currency> = {
    us_dollar: {
        name: 'US Dollar',
        namePlural: 'US Dollars',
        code: 'USD',
        symbol: '$',
        locale: 'en-US'
    }
} as const

export type Cart = {
    items: Record<string, CartItem>
    total: number
    currency: Currency
}

export type CartItem = { product: Product; amount: number }

/*******/
/* App */
/*******/

export type MessageLevel =
    | 'info'
    | 'success'
    | 'warning'
    | 'error'
    | 'purple'
    | 'secondary'
    | 'primary'

/* UI */
/*      Layout */
/*          Spacing */
export type Space =
    | 1
    | 2
    | 4
    | 8
    | 12
    | 16
    | 20
    | 24
    | 28
    | 32
    | 36
    | 40
    | 48
    | 60
    | 72
    | 100
    | 128
    | 256
    | 512
    | 1024
    | 2048
    | 3072
    | 4096
/*      Theme */
export type Theme = 'dark' | 'light'
/*      Colors */
/*          colors hue values */
export const COLORS = [
    'wild-watermelon',
    'picton-blue',
    'buttermilk-yellow',
    'rose-of-sharon',
    'mint-green',
    'shade'
] as const
export type Color = (typeof COLORS)[number]

/*          color tone/shade values */
export const SHADES = [
    50, 100, 200, 300, 400, 500, 600, 700, 800, 900, 950
] as const
export type Shade = (typeof SHADES)[number]

/* HTML */
/*      <input> */

/*              inputmode */
export const INPUTMODE_ATTRIBUTE_VALUES = [
    'none',
    'text',
    'tel',
    'url',
    'email',
    'numeric',
    'decimal',
    'search'
] as const // date, password ? :'(
export type InputmodeAttributeValue =
    (typeof INPUTMODE_ATTRIBUTE_VALUES)[number]
type InputmodeAttributes = {
    [K in InputmodeAttributeValue as `${Capitalize<string & K>}`]: K
}
export const INPUTMODE_ATTRIBUTES: InputmodeAttributes = {
    None: 'none',
    Text: 'text',
    Tel: 'tel',
    Url: 'url',
    Email: 'email',
    Numeric: 'numeric',
    Decimal: 'decimal',
    Search: 'search'
}

// eslint-disable-next-line  @typescript-eslint/no-explicit-any
export type RequestError = { e: any; message: string } & Record<string, any>
