import react from '@vitejs/plugin-react'
import copy from 'rollup-plugin-copy'
import { defineConfig } from 'vite'
import { VitePWA, VitePWAOptions } from 'vite-plugin-pwa'

const manifestForPlugin: Partial<VitePWAOptions> = {
    registerType: 'prompt',
    includeAssets: ['favicon.ico', 'apple-touch-icon.png', 'masked-icon.png'],
    manifest: {
        name: 'Gallery',
        short_name: 'Gallery',
        description:
            'links to galleries in google photos/drive, OneDrive/Albums, etc...',
        icons: [
            {
                src: '/assets/visuals/icons/app/app-icon-512x512.png',
                sizes: '512x512',
                type: 'image/png',
                purpose: 'any'
            },
            {
                src: '/assets/visuals/icons/app/maskable-icon-512x512.png',
                sizes: '512x512',
                type: 'image/png',
                purpose: 'maskable'
            }
            // TODO more icons
        ],
        theme_color: '#888', // TODO
        background_color: '#333', // TODO
        display: 'standalone',
        scope: '/',
        start_url: '/',
        orientation: 'portrait'
    }
}

export default defineConfig({
    plugins: [
        react({
            babel: {
                plugins: [['module:@preact/signals-react-transform']]
            }
        }),
        VitePWA(manifestForPlugin),
        copy({
            targets: [{ src: 'src/assets/*', dest: 'dist' }],
            verbose: true, // Optional, enables logging during the copy process
            flatten: false,
            hook: 'writeBundle'
        })
    ]
})
