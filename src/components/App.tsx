import React from 'react'
import { Navigate, RouterProvider, createBrowserRouter } from 'react-router-dom'

import { Products } from '.'
import { ThemeContextProvider } from '../store/Theme/ThemeContext'
import { Cart } from './Cart/Cart'
import { ErrorView } from './ErrorPage'
import { RootLayout } from './ui/layout/RootLayout'

const App: React.FC = () => {
    const router = createBrowserRouter([
        {
            path: '/',
            element: <RootLayout />,
            id: 'route-data',
            errorElement: <ErrorView />,
            children: [
                { path: '', element: <Navigate to="/products" /> },
                {
                    path: 'products',
                    element: <Products />,
                    id: 'products-data'
                },
                {
                    path: 'cart',
                    element: <Cart />,
                    id: 'cart'
                }
            ]
        }
    ])

    return (
        <ThemeContextProvider>
            <RouterProvider router={router} />
        </ThemeContextProvider>
    )
}

export default App
