import { act, render } from '@testing-library/react'
import { useContext } from 'react'
import { CURRENCIES, Cart, Product } from '../../model/model'
import { deepFreeze } from '../../util/object'
import CartContext, { CartContextProvider } from './CartContext'

const testProduct: Product = {
    id: 'test-product',
    name: 'Test Product',
    price: 7,
    availableAmount: 5,
    minOrderAmount: 3,
    img: ''
} as const
Object.freeze(testProduct)

const anotherTestProduct: Product = {
    id: 'another-test-product',
    name: 'Another Test Product',
    price: 11,
    availableAmount: 13,
    minOrderAmount: 1,
    img: ''
} as const
Object.freeze(anotherTestProduct)

const TestComponent = () => {
    const {
        cart,
        addProductToCart,
        removeProductFromCart,
        setItemAmount,
        increaseItemAmount,
        decreaseItemAmount
    } = useContext(CartContext)

    return (
        <>
            <button
                data-testid="ADD_PRODUCT"
                type="button"
                onClick={() => addProductToCart(testProduct, 4)}
            >
                Add
            </button>
            <button
                data-testid="ADD_TOO_MUCH"
                type="button"
                onClick={() => addProductToCart(testProduct, 8)}
            >
                Add too much
            </button>
            <button
                data-testid="ADD_TOO_FEW"
                type="button"
                onClick={() => addProductToCart(testProduct, 2)}
            >
                Add too much
            </button>
            <button
                data-testid="ADD_ANOTHER_PRODUCT"
                type="button"
                onClick={() => addProductToCart(anotherTestProduct, 2)}
            >
                Add too much
            </button>
            <button
                data-testid="REMOVE_PRODUCT"
                type="button"
                onClick={() => removeProductFromCart(testProduct.id)}
            >
                Add
            </button>
            <button
                data-testid="SET_AMOUNT"
                type="button"
                onClick={() => setItemAmount(testProduct.id, 3)}
            >
                Add
            </button>
            <button
                data-testid="INCREASE_AMOUNT"
                type="button"
                onClick={() => increaseItemAmount(testProduct.id)}
            >
                Add
            </button>
            <button
                data-testid="DECREASE_AMOUNT"
                type="button"
                onClick={() => decreaseItemAmount(testProduct.id)}
            >
                Add
            </button>
            <p data-testid="cart-value">{JSON.stringify(cart)}</p>
        </>
    )
}

const emptyCartState: Cart = {
    items: {},
    total: 0,
    currency: CURRENCIES.us_dollar
} as const
deepFreeze(emptyCartState)

const expectedCartState: Cart = {
    items: {
        [testProduct.id]: {
            product: testProduct,
            amount: 4
        }
    },
    total: testProduct.price * 4,
    currency: CURRENCIES.us_dollar
} as const
deepFreeze(expectedCartState)

beforeEach(() => localStorage.clear())

test('should initialize Cart', () => {
    const component = render(
        <CartContextProvider>
            <TestComponent />
        </CartContextProvider>
    )

    const actualCartValue = component.getByTestId('cart-value')?.textContent
    expect(actualCartValue).toEqual(JSON.stringify(emptyCartState))
})

test('should load state from localStorege', () => {
    const cartJson =
        '{"items":{"unique-test-product":{"product":{"id":"unique-test-product","name":"Test Product","price":7,"availableAmount":5,"minOrderAmount":3,"img":""},"amount":4}},"total":28,"currency":{"name":"US Dollar","namePlural":"US Dollars","code":"USD","symbol":"$","locale":"en-US"}}'
    localStorage.setItem('cart+guest', cartJson)

    const component = render(
        <CartContextProvider>
            <TestComponent />
        </CartContextProvider>
    )

    const actualCartValue = component.getByTestId('cart-value')?.textContent
    expect(actualCartValue).toEqual(JSON.stringify(JSON.parse(cartJson)))
})

test('should add a Product to Cart', () => {
    const expectedItemAmount = 4
    const expectedCartTotal = 28

    const expectedCartState: Cart = {
        items: {
            [testProduct.id]: {
                product: testProduct,
                amount: expectedItemAmount
            }
        },
        total: expectedCartTotal,
        currency: CURRENCIES.us_dollar
    }

    const component = render(
        <CartContextProvider>
            <TestComponent />
        </CartContextProvider>
    )
    act(() => component.getByTestId('ADD_PRODUCT').click())

    const actualCartValue = component.getByTestId('cart-value').textContent
    expect(JSON.parse(String(actualCartValue))).toEqual(expectedCartState)
})

test('should add another Product to Cart', () => {
    const expectedCartTotal = 50

    const component = render(
        <CartContextProvider>
            <TestComponent />
        </CartContextProvider>
    )
    act(() => component.getByTestId('ADD_PRODUCT').click())

    act(() => component.getByTestId('ADD_ANOTHER_PRODUCT').click())

    const actualCartValue = component.getByTestId('cart-value').textContent

    const actualCart = JSON.parse(String(actualCartValue))
    expect(actualCart.total).toEqual(expectedCartTotal)
})

test('should not perform the action when the Product is already in Cart', () => {
    const component = render(
        <CartContextProvider>
            <TestComponent />
        </CartContextProvider>
    )
    act(() => component.getByTestId('ADD_PRODUCT').click())

    act(() => component.getByTestId('ADD_PRODUCT').click())

    const actualCartValue = component.getByTestId('cart-value').textContent
    const actualCart = JSON.parse(String(actualCartValue))

    expect(actualCart).toEqual(expectedCartState)
})

test('should not perform the action when the available amount would be exceeded', () => {
    const component = render(
        <CartContextProvider>
            <TestComponent />
        </CartContextProvider>
    )

    act(() => component.getByTestId('ADD_TOO_MUCH').click())

    const actualCartValue = component.getByTestId('cart-value').textContent
    const actualCart = JSON.parse(String(actualCartValue))

    expect(actualCart).toEqual(emptyCartState)
})

test('should not perform the action when the minimum order amount would be violated', () => {
    const component = render(
        <CartContextProvider>
            <TestComponent />
        </CartContextProvider>
    )

    act(() => component.getByTestId('ADD_TOO_FEW').click())

    const actualCartValue = component.getByTestId('cart-value').textContent
    const actualCart = JSON.parse(String(actualCartValue))

    expect(actualCart).toEqual(emptyCartState)
})
