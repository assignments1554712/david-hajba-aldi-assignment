import React, { useContext, useMemo } from 'react'
import CartContext from '../../../store/Cart/CartContext'
import searchSignal from '../../../store/searchSignal'
import { getSortingCartItemByPrice } from '../../../util/domain'
import { useSorting } from '../../Hooks/sorting.hook'
import Price from '../../Price/Price'
import SortingButton from '../../SortingButton'
import { CartItemDisplay } from './CartItemDisplay'

interface IProductsProps {}

export const CartItems: React.FC<IProductsProps> = () => {
    const { cart } = useContext(CartContext)

    const { sorting, sortingDirection, toggleSortingDirection } = useSorting(
        getSortingCartItemByPrice
    )

    const cartItems = Object.values(cart.items)
    let listedCartItems = useMemo(() => {
        let cartItemEntries = Object.values(cartItems)
        if (searchSignal.value) {
            cartItemEntries = cartItemEntries.filter((cartItem) => {
                const content =
                    `${cartItem.product.name} ${cartItem.product.minOrderAmount} ${
                        cartItem.product.unit?.abbrPlural || ''
                    }`.toLocaleLowerCase()
                return searchSignal.value
                    .split(' ')
                    .some((searchValue) =>
                        new RegExp(searchValue).test(content)
                    )
            })
        }
        return cartItemEntries
        // yes, mutating searchSignal.value re-renders the component
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [cartItems, searchSignal.value])

    listedCartItems = listedCartItems.sort(sorting)

    return (
        <div className="p-3">
            <div className="flex justify-between py-4">
                <h3>Cart</h3>
                <h4>
                    Total:{' '}
                    <Price
                        price={Object.values(cartItems).reduce(
                            (acc, curr) =>
                                acc + curr.amount * curr.product.price,
                            0
                        )}
                    />
                </h4>
            </div>
            <div className="flex justify-end py-4">
                <SortingButton
                    {...{
                        sortingAttributeName: 'Price',
                        sortingDirection,
                        currentListLength: listedCartItems.length,
                        onClick: toggleSortingDirection
                    }}
                />
            </div>
            <ul className="flex flex-col gap-4">
                {listedCartItems.map((cartItem) => (
                    <CartItemDisplay
                        cartItem={cartItem}
                        key={cartItem.product.id}
                    />
                ))}
            </ul>
        </div>
    )
}
