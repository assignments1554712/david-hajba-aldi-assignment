import { ReactNode, createContext, useEffect, useReducer } from 'react'
import { Theme } from '../../model/model'
import { setGlobalTheme } from '../../util/theme'

type ThemeAction =
    | {
          type: 'SET_THEME'
          payload: { theme: Theme }
      }
    | {
          type: 'TOGGLE_THEME'
          payload: Record<string, never>
      }

const ThemeContext = createContext<{
    theme: Theme
    setTheme: (theme: Theme) => void
    toggleTheme: () => void
}>({
    theme: 'light',
    setTheme: (theme: Theme) =>
        console.warn(
            '[ThemeContext.tsx] Invoked the initial action for Context ThemeCtxt > setTheme: (theme: Theme)',
            theme
        ),
    toggleTheme: () =>
        console.warn(
            '[ThemeContext.tsx] Invoked the initial action for Context ThemeCtxt > toggleTheme: ()'
        )
})

const ThemeContextProvider = ({ children }: { children: ReactNode }) => {
    const [themeState, themeDispatch] = useReducer<
        (state: Theme, action: ThemeAction) => Theme
    >(themeReducer, 'light')

    useEffect(() => {
        const theme: Theme | undefined =
            (localStorage.getItem('last-theme-preference') as Theme) ??
            undefined
        setTimeout(() => setGlobalTheme(theme, true), theme ? 0 : 100)
    }, [])

    function handleSetTheme(theme: Theme) {
        themeDispatch({
            type: 'SET_THEME',
            payload: { theme }
        })
    }
    function handleToggleTheme() {
        themeDispatch({
            type: 'TOGGLE_THEME',
            payload: {}
        })
    }

    return (
        <ThemeContext.Provider
            value={{
                theme: themeState,
                setTheme: handleSetTheme,
                toggleTheme: handleToggleTheme
            }}
        >
            {children}
        </ThemeContext.Provider>
    )
}

export default ThemeContext
export { ThemeContextProvider }

function themeReducer(
    state: Theme,
    { type: actionType, payload }: ThemeAction
): Theme {
    let newThemeState: Theme = structuredClone(state)

    if (actionType === 'SET_THEME') {
        newThemeState = payload.theme
        setGlobalTheme(newThemeState)
    } else if (actionType === 'TOGGLE_THEME') {
        newThemeState = newThemeState === 'dark' ? 'light' : 'dark'
        setGlobalTheme(newThemeState)
    } else {
        throw (
            '[ThemeContext] Wrong action, this should never happen - ' +
            '"' +
            actionType +
            '"'
        )
    }

    return newThemeState
}
