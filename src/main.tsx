import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './components/App.tsx'
import './index.scss'

export const BUILD = 'initial-squirrell'

ReactDOM.createRoot(document.getElementById('root')!).render(
    <React.StrictMode>
        <App />
    </React.StrictMode>
)
