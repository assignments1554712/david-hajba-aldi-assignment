module.exports = {
    root: true,
    env: { browser: true, es2020: true },
    extends: [
        'eslint:recommended',
        'plugin:@typescript-eslint/recommended',
        'plugin:react-hooks/recommended',
        'plugin:storybook/recommended'
    ],
    ignorePatterns: [
        'dist',
        '.eslintrc.cjs',
        'src/lib',
        'src/util',
        'functions'
    ],
    parser: '@typescript-eslint/parser',
    plugins: ['react-refresh', 'unused-imports'],
    rules: {
        'react-refresh/only-export-components': [
            'warn',
            { allowConstantExport: true }
        ],
        'unused-imports/no-unused-imports': ['error'],
        semi: ['error', 'never'],
        'semi-style': ['error', 'first']
    }
}
