export type Alter<T, R> = Pick<T, Exclude<keyof T, keyof R>> & R
