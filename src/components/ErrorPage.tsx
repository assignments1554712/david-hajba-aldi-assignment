import React from 'react'
import { useNavigate } from 'react-router-dom'
import Header from './ui/layout/Header'

export const ErrorView: React.FC = () => {
    const navigate = useNavigate()

    return (
        <div className="w-screen h-screen flex flex-col">
            <Header />
            <div className="flex-grow flex flex-col items-center justify-center">
                <div className="flex items-start">
                    <h1 className="inline-block text-error">Error</h1>
                    <pre className="inline-block">Something's not working</pre>
                </div>
                <a onClick={() => navigate('/')} className="mt-16 text-info">
                    Home
                </a>
            </div>
        </div>
    )
}
