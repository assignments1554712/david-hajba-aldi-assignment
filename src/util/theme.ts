import { Theme } from '../model/model'

export function setGlobalTheme(theme: Theme = 'light', kind = false) {
    if (!document.body?.parentElement) return
    if (
        kind &&
        /\b(dark|light)\b/.test(
            document.body.parentElement.classList.toString()
        )
    )
        return

    const themeInvert: { [keys in Theme]: Theme } = {
        dark: 'light',
        light: 'dark'
    }
    document.body.parentElement.classList.add(theme)
    document.body.parentElement.classList.remove(themeInvert[theme])
    localStorage.setItem('last-theme-preference', theme)
}
