import React, { useContext } from 'react'
import { useNavigate } from 'react-router-dom'
import { OrderAmount, ProductDisplay } from '../..'
import { Product } from '../../../model/model'
import CartContext from '../../../store/Cart/CartContext'
import AmountControl from '../../AmountControl'

interface IProductDetailProps {
    product: Product
}

export const ProductListItemDisplay: React.FC<IProductDetailProps> = ({
    product
}) => {
    const { cart, addProductToCart } = useContext(CartContext)
    const cartItem = cart.items[product.id] || null

    const navigate = useNavigate()

    return (
        <ProductDisplay
            product={product}
            cartItem={cartItem}
            renderControls={(
                availableInMinOrderAmount: boolean,
                remainingAvailableAmount: number
            ) => {
                return (
                    <>
                        {cartItem ? (
                            <span className="w-full flex items-center justify-between gap-4">
                                <span>
                                    <OrderAmount amount={cartItem.amount} /> in
                                    Cart
                                </span>
                                <button
                                    type="button"
                                    onClick={() => navigate('/cart')}
                                    className="btn btn-primary form-control-compact"
                                >
                                    Go to Cart
                                </button>
                            </span>
                        ) : (
                            <AmountControl
                                {...{
                                    product,
                                    onSubmit: (newAmount: number) =>
                                        addProductToCart(product, newAmount),
                                    submitText: 'Add to Cart',
                                    remainingAvailableAmount,
                                    availableInMinOrderAmount
                                }}
                            />
                        )}
                    </>
                )
            }}
        />
    )
}
