import React from 'react'
import { ORDER_UNITS, OrderUnit } from '../../model/model'
import { orderAmount } from '../../util/domain'

interface IOrderAmountProps {
    amount: number
    unit?: OrderUnit
}

export const OrderAmount: React.FC<IOrderAmountProps> = ({
    amount,
    unit = ORDER_UNITS.piece
}) => {
    return orderAmount(amount, unit)
}
