import { deepFreeze } from './object'

let a: string[]
beforeEach(() => (a = ['x']))

test('should throw when extend at level 0', () => {
    const extendA = () => a.push('y')

    deepFreeze(a)

    expectToThrow(extendA)
    expect(a).toEqual(['x'])
})

test('should throw whwn mutate at level 0', () => {
    const mutateA = () => (a[0] = 'y')

    deepFreeze(a)

    expectToThrow(mutateA)
    expect(a).toEqual(['x'])
})

function expectToThrow(call: () => void, errorClass = TypeError) {
    expect(call).toThrow(errorClass)
}
