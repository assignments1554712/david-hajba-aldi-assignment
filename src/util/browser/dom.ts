export function classNames(...classesses: string[]) {
    // return classesses.map(x => x.trim().replace(/\s+/g, ' ')).filter(Boolean).join(' ')
    return classesses
        .reduce(
            (acc, curr) => {
                const trimmed = processClassName(curr)
                return trimmed ? acc.concat(' ' + trimmed) : acc
            },
            processClassName(classesses.shift() || '')
        )
        .trim()
}

function processClassName(curr: string) {
    return curr.trim().replace(/\s+/g, ' ')
}

export function classNames2(classesses: Record<string, boolean>) {
    return Object.entries(classesses)
        .reduce<string[]>(
            (acc, [classItem, active]) => [
                ...acc,
                ...(active ? [classItem.trim()] : [])
            ],
            []
        )
        .join(' ')
}
