import React, { useContext } from 'react'
import { CartItem } from '../../../model/model'
import CartContext from '../../../store/Cart/CartContext'
import AmountControl from '../../AmountControl'
import { ProductDisplay } from '../../Product/ProductDisplay/ProductDisplay'

interface IProductDetailProps {
    cartItem: CartItem
}

export const CartItemDisplay: React.FC<IProductDetailProps> = ({
    cartItem
}) => {
    const {
        increaseItemAmount,
        decreaseItemAmount,
        setItemAmount,
        removeProductFromCart
    } = useContext(CartContext)
    return (
        <ProductDisplay
            product={cartItem.product}
            cartItem={cartItem}
            renderControls={(
                availableInMinOrderAmount: boolean,
                // eslint-disable-next-line @typescript-eslint/no-unused-vars
                remainingAvailableAmount: number
            ) => {
                return (
                    <>
                        <AmountControl
                            {...{
                                product: cartItem.product,
                                onIncrease: () =>
                                    increaseItemAmount(cartItem.product.id),
                                onDecrease: () =>
                                    decreaseItemAmount(cartItem.product.id),
                                onSubmit: (newAmount: number) =>
                                    setItemAmount(
                                        cartItem.product.id,
                                        newAmount
                                    ),
                                submitText: 'Change Amount',
                                remainingAvailableAmount:
                                    cartItem.product.availableAmount,
                                availableInMinOrderAmount,
                                cartItemAmount: cartItem.amount
                            }}
                        />
                    </>
                )
            }}
            actionsElement={
                /* Defer remove action for confirmation */
                <button
                    onClick={() => removeProductFromCart(cartItem.product.id)}
                    className="btn form-control-transparent"
                >
                    Remove
                </button>
            }
        />
    )
}
