/* eslint-disable @typescript-eslint/no-explicit-any */

export function assignNestedProperty(
    obj: Record<any, any>,
    path: string,
    value: any
) {
    if (!path.match(/^([a-zA-Z])([\w_-]*(\.[\w_-]*))?$/))
        throw new Error('Invalid property path')
    const keys = path.split('.')
    const lastKey = keys.pop()
    const lastObj = keys.reduce((acc, key) => {
        acc[key] = acc[key] || {}
        return acc[key]
    }, obj)

    lastObj[lastKey!] = value
}

export function traverse<T = any>(
    obj: Record<any, any>,
    callback: (value: T) => T
) {
    // console.log(Object.entries(obj))
    Object.values(obj).forEach((v) => {
        if (v && typeof v === 'object') traverse(v, callback)
        callback(v)
    })
    callback(obj)
}

export const deepFreeze = (obj: Record<any, any>) =>
    traverse(obj, deepFreezeCallback)

function deepFreezeCallback<T = any>(value: T): T {
    if (typeof value === 'object' && !Object.isFrozen(value))
        Object.freeze(value)
    return value
}
