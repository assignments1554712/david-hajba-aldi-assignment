import React from 'react'
import splash from '../../../../assets/visuals/icons/app/splash.svg'
import { classNames } from '../../../../util/browser/dom'
import Spinner from '../../elements/Spinner/Spinner'
import styles from './SplashScreen.module.scss'

const SplashScreen: React.FC = () => (
    <div
        id="splash-screen"
        style={{ backgroundImage: `url(${splash})` }}
        className={classNames(
            'fixed top-0 left-0 h-screen w-full',
            styles.SplashScreen
        )}
    >
        <div className="absolute w-full top-2/3 flex justify-center">
            <Spinner />
        </div>
    </div>
)

export default SplashScreen
