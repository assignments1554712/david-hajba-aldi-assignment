import { Signal } from '@preact/signals-react'
import React, { useRef, useState } from 'react'

export const DebouncedInput: React.FC<
    {
        debounce?: number
        onChange: (value: string) => void
        signal?: Signal
        placeholder?: string
    } & React.InputHTMLAttributes<HTMLInputElement>
> = ({ debounce = 300, onChange, signal, placeholder = '', ...props }) => {
    const [value, setValue] = useState(signal?.value || '')
    const timerId = useRef<number | undefined>()

    const handleChange = ({ target }: { target: HTMLInputElement }) => {
        const value: string = target?.value
        setValue(value)

        timerId.current && clearTimeout(timerId.current)
        const timer = setTimeout(() => {
            onChange(value)
            signal && (signal.value = value)
        }, debounce)
        timerId.current = timer as unknown as number
    }

    return (
        <div>
            <input
                {...props}
                value={value}
                onChange={handleChange}
                className="form-control-compact"
                placeholder={placeholder}
            />
        </div>
    )
}
