import { ChangeEvent, useState } from 'react'
import { Product } from '../model/model'
import { orderAmount } from '../util/domain'

type AmountControlProps = {
    product: Product
    onIncrease?: () => void
    onDecrease?: () => void
    onAmountChange?: (newAmount: number) => void
    onSubmit: (newAmount: number) => void
    submitText: string
    remainingAvailableAmount: number
    availableInMinOrderAmount: boolean
    cartItemAmount?: number
}

const AmountControl: React.FC<AmountControlProps> = ({
    product,
    onIncrease,
    onDecrease,
    onAmountChange,
    onSubmit,
    submitText,
    remainingAvailableAmount,
    availableInMinOrderAmount,
    cartItemAmount = NaN
}) => {
    const [amount, setAmount] = useState(product.minOrderAmount)

    const availableAmountExceeded = amount > remainingAvailableAmount
    const minOrderAmountViolated = amount < product.minOrderAmount
    const submitDisabled =
        availableAmountExceeded ||
        minOrderAmountViolated ||
        !availableInMinOrderAmount

    const handleAmountChange = (newAmount: number) => {
        setAmount(newAmount)
        onAmountChange && onAmountChange(newAmount)
    }
    const handleDecrease = () => {
        setAmount((x) => x - 1)
        onDecrease && onDecrease()
    }
    const handleIncrease = () => {
        setAmount((x) => x + 1)
        onIncrease && onIncrease()
    }

    return (
        <>
            <span className="w-full flex flex-wrap items-end justify-end gap-4">
                <span>
                    <span className="w-full flex items-center gap-2">
                        <button
                            type="button"
                            onClick={handleDecrease}
                            disabled={amount <= (product.minOrderAmount || 1)}
                            className="btn btn-secondary form-control-compact"
                            data-testid="amount-control-decrease"
                        >
                            -
                        </button>
                        <input
                            type="number"
                            value={amount}
                            onChange={({
                                target
                            }: ChangeEvent<HTMLInputElement>) =>
                                handleAmountChange(Number(target.value))
                            }
                            max={product.availableAmount || Infinity}
                            min={product.minOrderAmount || 1}
                            className="w-20 form-control-inline-block form-control-compact"
                        />
                        <button
                            type="button"
                            onClick={handleIncrease}
                            disabled={amount >= remainingAvailableAmount}
                            className="btn btn-secondary form-control-compact"
                            data-testid="amount-control-increase"
                        >
                            +
                        </button>
                    </span>
                </span>
                <button
                    type="button"
                    onClick={() => onSubmit(amount)}
                    disabled={submitDisabled || amount === cartItemAmount}
                    className="btn btn-info form-control-inline-block form-control-compact"
                    title={
                        submitDisabled
                            ? `Please check the minimum -and available amount`
                            : `Add ${orderAmount(amount)} to the Cart`
                    }
                    data-testid="amount-control-submit"
                >
                    {submitText}
                </button>
            </span>
        </>
    )
}

export default AmountControl
