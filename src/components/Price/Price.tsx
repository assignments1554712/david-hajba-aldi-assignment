import { CURRENCIES, Currency } from '../../model/model'

type PriceProps = {
    price: number
    currency?: Currency
}

const Price: React.FC<PriceProps> = ({
    price,
    currency = CURRENCIES.us_dollar
}) => (
    <>
        {Intl.NumberFormat(currency.locale, {
            style: 'currency',
            currency: currency.code
        }).format(price)}
    </>
)

export default Price
