import { CURRENCIES, ORDER_UNITS, Product } from '../../model/model'
import Price from './Price'

type ProductPriceProps = {
    product: Product
}

const ProductPrice: React.FC<ProductPriceProps> = ({ product }) => (
    <span className="flex items-end">
        <strong className="text-lg mr-1">
            <Price
                price={product.price}
                currency={product.currency || CURRENCIES.us_dollar}
            />
        </strong>
        /{(product.unit || ORDER_UNITS.piece).abbr}
    </span>
)

export default ProductPrice
