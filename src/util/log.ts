/* eslint-disable @typescript-eslint/no-explicit-any */
export const todo = (message: string, ...args: any[]) =>
    console.log(`%c// TODO ${message}`, 'color: #248139', ...args)
