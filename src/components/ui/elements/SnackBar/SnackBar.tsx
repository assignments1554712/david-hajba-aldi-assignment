import React from 'react'
import { MessageLevel } from '../../../../model/model'

type SnackBarProps = {
    onDismissed: () => void
    message?: string
    level: MessageLevel
}

const SnackBar: React.FC<SnackBarProps> = ({
    message = '',
    level = 'primary',
    onDismissed
}) => (
    <>
        {message && (
            <div
                className={`fixed bottom-12 left-0 px-4 py-2 w-full text-center flex justify-between gap-4 bg-${level}`}
            >
                <span>{/* For centering */}</span>
                <span>{message}</span>
                <span className="cursor-pointer" onClick={onDismissed}>
                    X
                </span>
            </div>
        )}
    </>
)

export default SnackBar
