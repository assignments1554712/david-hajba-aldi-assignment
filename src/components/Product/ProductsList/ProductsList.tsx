import React, { useContext, useMemo } from 'react'
import ProductsListContext from '../../../store/ProductsList/ProductsListContext.tsx'
import searchSignal from '../../../store/searchSignal.ts'
import { getSortingProductByPrice } from '../../../util/domain.ts'
import { useSorting } from '../../Hooks/sorting.hook.ts'
import SortingButton from '../../SortingButton.tsx'
import { DebouncedInput } from '../../ui/form/form-controls/DebouncedInput.tsx'
import SplashScreen from '../../ui/icon/SplashScreen/SplashScreen.tsx'
import { ProductListItemDisplay } from './ProductsListItemDisplay.tsx'

interface IProductsListProps {}

export const ProductsList: React.FC<IProductsListProps> = () => {
    const { productsList: products } = useContext(ProductsListContext)

    const { sorting, sortingDirection, toggleSortingDirection } = useSorting(
        getSortingProductByPrice
    )

    let listedProducts = useMemo(() => {
        let cartItemEntries = Object.values(products)
        if (searchSignal.value) {
            cartItemEntries = cartItemEntries.filter((product) => {
                const content = `${product.name} ${product.minOrderAmount} ${
                    product.unit?.abbrPlural || ''
                }`.toLocaleLowerCase()
                return searchSignal.value
                    .split(' ')
                    .some((searchValue) =>
                        new RegExp(searchValue).test(content)
                    )
            })
        }
        return cartItemEntries
        // yes, mutating searchSignal.value re-renders the component
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [products, searchSignal.value])

    listedProducts = listedProducts.sort(sorting)

    if (!products.length) return <SplashScreen />

    return (
        <div className="p-3">
            <div className="flex justify-between py-4">
                <h3>Products</h3>
                <SortingButton
                    {...{
                        sortingAttributeName: 'Product',
                        sortingDirection,
                        currentListLength: listedProducts.length,
                        onClick: toggleSortingDirection
                    }}
                />
            </div>
            <div className="flex justify-end mb-3">
                <DebouncedInput
                    type="search"
                    disabled={false}
                    onChange={(value) => {
                        searchSignal.value = value as string
                    }}
                    placeholder="filter"
                />
            </div>
            <ul className="flex flex-col gap-4">
                {listedProducts.map((product) => (
                    <ProductListItemDisplay
                        product={product}
                        key={product.id}
                    />
                ))}
            </ul>
        </div>
    )
}
