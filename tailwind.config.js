/** @type {import('tailwindcss').Config} */
export default {
    content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
    darkMode: 'class',
    plugins: [],
    theme: {
        screens: {
            xxs: '320px',
            xs: '480px',
            sm: '640px',
            md: '768px',
            lg: '1024px',
            xl: '1280px',
            '2xl': '1536px'
        },
        extend: {
            aspectRatio: {
                '3/2': '3 / 2'
            }
        },
        colors: {
            'wild-watermelon': {
                50: '#fff1f2',
                100: '#ffe3e5',
                200: '#ffb8bf',
                300: '#ff8997',
                400: '#ff576d',
                500: '#f93a58',
                600: '#e71742',
                700: '#c30d36',
                800: '#a30e35',
                900: '#8b1034',
                950: '#4e0317'
            },
            'picton-blue': {
                50: '#eff8ff',
                100: '#dfefff',
                200: '#b8e0ff',
                300: '#4cb7ff',
                400: 'rgb(var(--color-picton-blue-400))',
                500: '#0692f1',
                600: '#0073ce',
                700: '#005ca7',
                800: '#024d8a',
                900: '#084172',
                950: '#06294b'
            },
            carnation: {
                50: '#fff1f2',
                100: '#ffe1e4',
                200: '#ffc7cd',
                300: '#ffa0aa',
                400: '#ff5768',
                500: '#f83b4e',
                600: '#e51d31',
                700: '#c11426',
                800: '#a01422',
                900: '#841823',
                950: '#48070e'
            },
            'buttermilk-yellow': {
                50: '#fff2c2',
                100: '#fff0b8',
                200: '#fcedb0',
                300: '#fadd75',
                400: '#f8cf49',
                500: '#f2b531',
                600: '#f49d10',
                700: '#d2730f',
                800: '#ac5e15',
                900: '#925216',
                950: '#643407'
            },
            'rose-of-sharon': {
                50: '#fdf9e7',
                100: '#faf0c7',
                200: '#f5df8e',
                300: '#efcc57',
                400: '#ebb62d',
                500: '#de941b',
                600: '#c67415',
                700: '#bd580a',
                800: '#953c09',
                900: '#79320b',
                950: '#461901'
            },
            'mint-green': {
                50: '#eefff5',
                100: '#d8ffeb',
                200: '#b8ffda',
                300: '#78fdb9',
                400: '#36f293',
                500: '#0cdb73',
                600: '#03b65c',
                700: '#078e4a',
                800: '#0b703e',
                900: '#0c5b35',
                950: '#00331c'
            },
            'perfume-purple': {
                50: '#f9f5ff',
                100: '#f1e8ff',
                200: '#e5d4ff',
                300: '#d4b8ff',
                400: '#b582fe',
                500: '#9953f9',
                600: '#8231ec',
                700: '#6e20d0',
                800: '#5e1faa',
                900: '#4e1b88',
                950: '#320665'
            },
            shade: {
                0: '#FFFFFF',

                50: 'rgb(var(--color-shade-50))',
                100: 'rgb(var(--color-shade-100))',
                150: '#DBDBDB',
                200: '#CFCFCF',
                250: '#C3C3C3',
                300: '#B7B7B7',
                350: '#ABABAB',
                400: '#9F9f9f',
                450: '#939393',
                500: '#878787',
                550: '#7B7B7B',
                600: '#6f6f6f',
                650: '#636363',
                700: '#575757',
                750: '#4B4B4B',
                800: '#3F3F3F',
                850: '#333333',
                900: 'rgb(var(--color-shade-900))',
                950: '#1B1B1B',
                1000: 'rgb(var(--color-shade-1000))',

                Infinity: '#000000'
            },
            transparent: {
                // See usage in ui-color.scss
                100: '#E7E7E7', // transparent active light background
                200: 'transparent', // transparent light background
                150: '#DBDBDB', // transparent dark foreground
                850: '#333333', // transparent light foreground
                950: '#1B1B1B', // transparent active dark background
                1050: 'transparent' //transparent dark background
            }
        }
    }
}
