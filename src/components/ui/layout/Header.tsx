import React, { useContext } from 'react'
import { useNavigate } from 'react-router-dom'
import CartContext from '../../../store/Cart/CartContext'
import { classNames, classNames2 } from '../../../util/browser/dom'
import { Menu } from '../Menu/Menu'

const Header: React.FC = () => {
    const { cart } = useContext(CartContext)
    const navigate = useNavigate()

    const itemCount = Object.keys(cart.items).length

    return (
        <>
            <div className="flex justify-between p-4">
                <div className="flex gap-3">
                    <Menu></Menu>
                    <h4>Aldi</h4>
                </div>
                <a
                    onClick={() => navigate('/cart')}
                    className={classNames(
                        'relative decoration-transparent-200 btn btn-primary form-control-compact',
                        classNames2({ 'm-4': !!itemCount })
                    )}
                >
                    <span>Cart</span>
                    {!!itemCount && (
                        <div className="badge w-6 h-6 p-2 absolute -top-2 -right-6 inline-flex m-2 items-center justify-center rounded-full border-thin border-shade-50 dark:border-shade-950 bg-picton-blue-600">
                            <strong className="text-picton-blue-50 text-sm">
                                {itemCount}
                            </strong>
                        </div>
                    )}
                </a>
            </div>
        </>
    )
}

export default Header
