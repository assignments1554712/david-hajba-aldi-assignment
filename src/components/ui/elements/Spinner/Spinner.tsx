import { Color, Shade } from '../../../../model/model'
import { classNames, classNames2 } from '../../../../util/browser/dom'
import './Spinner.scss'

type SpinnerProps = {
    className?: string
    size?: number
    color?: {
        color: Color
        shade: Shade
    }
}

/**
 * Size <= 0: format directly
 */
const Spinner: React.FC<SpinnerProps> = ({
    className = '',
    size,
    color,
    ...props
}) => (
    <div
        className={classNames(
            'spinner-wrapper',
            classNames2({
                'w-8 h-8': size === undefined,
                [`w-${size} h-${size}`]: size !== undefined && size > 0
            })
        )}
    >
        <div
            className={classNames(
                'spinner inline-block rounded-full',
                color
                    ? `border-${color?.color}-${color?.shade}`
                    : 'dark:border-shade-100 border-shade-950',
                classNames2({
                    'w-8 h-8': size === undefined,
                    [`w-${size} h-${size}`]: size !== undefined && size > 0
                }),
                className
            )}
            {...props}
        ></div>
    </div>
)

export default Spinner
