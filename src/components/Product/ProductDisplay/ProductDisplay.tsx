import React, { ReactNode } from 'react'
import Highlighter from 'react-highlight-words'
import { CartItem, Product } from '../../../model/model'
import searchSignal from '../../../store/searchSignal'
import { orderAmount } from '../../../util/domain'
import Price from '../../Price/Price'
import ProductPrice from '../../Price/ProductPrice'
import Image from '../../ui/elements/Image/Image'

interface IProductDetailProps {
    product: Product
    cartItem: CartItem | null
    renderControls: (
        availableInMinOrderAmount: boolean,
        remainingAvailableAmount: number
    ) => ReactNode
    actionsElement?: ReactNode
}

export const ProductDisplay: React.FC<IProductDetailProps> = ({
    product,
    cartItem,
    renderControls,
    actionsElement
}) => {
    const availableInMinOrderAmount =
        product.availableAmount >= product.minOrderAmount
    const remainingAvailableAmount =
        product.availableAmount - (cartItem?.amount || 0)

    return (
        <li
            className="p-4 bg-card flex flex-col xs:flex-row xs:items-stretch gap-6 min-w-80 xs:min-w-96"
            key={product.id}
        >
            <Image imgSrc={product.img} product={true} />
            <article className="w-full flex flex-col">
                <div className="product-attributes">
                    <div className="flex items-center justify-between">
                        <h3 className="text-primary mb-4">
                            <Highlighter
                                searchWords={searchSignal.value.split(' ')}
                                autoEscape={true}
                                textToHighlight={product.name}
                            />
                        </h3>
                        {!!actionsElement && actionsElement}
                    </div>
                    <div>
                        {product.availableAmount &&
                        availableInMinOrderAmount ? (
                            <>
                                <Highlighter
                                    searchWords={searchSignal.value.split(' ')}
                                    autoEscape={true}
                                    textToHighlight={orderAmount(
                                        remainingAvailableAmount,
                                        product.unit
                                    )}
                                />{' '}
                                available
                            </>
                        ) : (
                            <span className="text-error">Out of stock</span>
                        )}
                    </div>
                    <div>
                        {product.minOrderAmount > 1 && (
                            <span className="text-warning">
                                Minimum order amount:
                                <strong>
                                    <Highlighter
                                        searchWords={searchSignal.value.split(
                                            ' '
                                        )}
                                        autoEscape={true}
                                        textToHighlight={orderAmount(
                                            product.minOrderAmount,
                                            product.unit
                                        )}
                                    />
                                    (
                                    <Price
                                        price={
                                            product.minOrderAmount *
                                            product.price
                                        }
                                        currency={product.currency}
                                    />
                                    )
                                </strong>
                            </span>
                        )}
                    </div>
                </div>
                <div className="flex-grow"></div>
                <div className="flex justify-end">
                    <span className="py-2 text-primary flex flex-row-reverse--depending-on-currency">
                        <ProductPrice product={product} />
                    </span>
                </div>
                <div className="product-controls flex items-center justify-end gap-2 xs:mt-0 mt-4">
                    {renderControls(
                        availableInMinOrderAmount,
                        remainingAvailableAmount
                    )}
                </div>
            </article>
        </li>
    )
}
