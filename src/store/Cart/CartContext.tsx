import { ReactNode, createContext, useEffect, useReducer } from 'react'
import { CURRENCIES, Cart, Product } from '../../model/model'

type CartActon =
    | {
          type: 'INITIALIZE_CART'
          payload: {
              cart: Cart
          }
      }
    | {
          type: 'ADD_PRODUCT_TO_CART'
          payload: {
              product: Product
              amount: number
          }
      }
    | {
          type: 'REMOVE_PRODUCT_FROM_CART'
          payload: {
              productId: string
          }
      }
    | {
          type: 'INCREASE_ITEM_AMOUNT'
          payload: {
              productId: string
          }
      }
    | {
          type: 'DECREASE_ITEM_AMOUNT'
          payload: {
              productId: string
          }
      }
    | {
          type: 'SET_ITEM_AMOUNT'
          payload: {
              productId: string
              amount: number
          }
      }

const CartContext = createContext<{
    cart: Cart
    addProductToCart: (product: Product, amount?: number) => void
    removeProductFromCart: (productId: string) => void
    increaseItemAmount: (productId: string) => void
    decreaseItemAmount: (productId: string) => void
    setItemAmount: (productId: string, amount: number) => void
}>({
    cart: { items: {}, total: 0, currency: CURRENCIES.us_dollar },
    addProductToCart: (product, amount = 1) =>
        console.warn(
            '[CartContext.tsx] Invoked the initial action for Context CartCtxt > addProductToCart(product: Product, amount: number)',
            product,
            amount
        ),
    removeProductFromCart: (productId) =>
        console.warn(
            '[CartContext.tsx] Invoked the initial action for Context CartCtxt > removeProductFromCart(gallery: Gallery)',
            productId
        ),
    increaseItemAmount: (productId) =>
        console.warn(
            '[CartContext.tsx] Invoked the initial action for Context CartCtxt > increaseItemAmount(gallery: Gallery)',
            productId
        ),
    decreaseItemAmount: (productId) =>
        console.warn(
            '[CartContext.tsx] Invoked the initial action for Context CartCtxt > decreaseItemAmount(cart: Cart, persist?: boolean)',
            productId
        ),
    setItemAmount: (productId, amount) =>
        console.warn(
            '[CartContext.tsx] Invoked the initial action for Context CartCtxt > setItemAmount(productId: string, amount: number)',
            productId,
            amount
        )
})

const CartContextProvider = ({ children }: { children: ReactNode }) => {
    const [cartState, cartDispatch] = useReducer<
        (state: Cart, action: CartActon) => Cart
    >(cartReducer, { items: {}, total: 0, currency: CURRENCIES.us_dollar })

    useEffect(() => {
        try {
            const cart = JSON.parse(String(localStorage.getItem(`cart+guest`)))

            // TODO validate and update cart state with respect to the current product list state (validate available/min amounts)

            cart && handleInitializeCart(cart)
        } catch (e) {
            console.debug('[CartContext] Could not parse localStorage entry')
        }
    }, [])

    function handleInitializeCart(cart: Cart) {
        cartDispatch({
            type: 'INITIALIZE_CART',
            payload: { cart }
        })
    }
    function handleIncreaseItemAmount(productId: string) {
        cartDispatch({
            type: 'INCREASE_ITEM_AMOUNT',
            payload: { productId }
        })
    }
    function handleAddProductToCart(product: Product, amount = 1) {
        cartDispatch({
            type: 'ADD_PRODUCT_TO_CART',
            payload: { product, amount }
        })
    }
    function handleRemoveProductFromCart(productId: string) {
        cartDispatch({
            type: 'REMOVE_PRODUCT_FROM_CART',
            payload: { productId }
        })
    }
    function handleDecreaseItemAmount(productId: string) {
        cartDispatch({
            type: 'DECREASE_ITEM_AMOUNT',
            payload: { productId }
        })
    }
    function handleSetItemAmount(productId: string, amount: number) {
        cartDispatch({
            type: 'SET_ITEM_AMOUNT',
            payload: { productId, amount }
        })
    }

    return (
        <CartContext.Provider
            value={{
                cart: cartState,
                increaseItemAmount: handleIncreaseItemAmount,
                addProductToCart: handleAddProductToCart,
                removeProductFromCart: handleRemoveProductFromCart,
                decreaseItemAmount: handleDecreaseItemAmount,
                setItemAmount: handleSetItemAmount
            }}
        >
            {children}
        </CartContext.Provider>
    )
}

export default CartContext
export { CartContextProvider }

function cartReducer(
    state: Cart,
    { type: actionType, payload }: CartActon
): Cart {
    let newCartState: Cart = structuredClone(state)

    try {
        if (actionType === 'INITIALIZE_CART') {
            newCartState = payload.cart
        } else if (actionType === 'ADD_PRODUCT_TO_CART') {
            if (newCartState.items[payload.product.id])
                throw '[CartContext] The cart already has this product'
            if (payload.product.availableAmount < payload.amount)
                throw '[CartContext] Cannot add product to Cart, because the available amount is exceeded'
            if (payload.product.minOrderAmount > payload.amount)
                throw '[CartContext] Cannot add product to Cart, because the minimum order amount would be violated'

            const product = structuredClone(payload.product)
            const cartItem = { product, amount: payload.amount }

            newCartState.items[product.id] = cartItem
            newCartState.total += payload.amount * product.price
        } else if (actionType === 'INCREASE_ITEM_AMOUNT') {
            const cartItem = newCartState.items[payload.productId]
            if (!cartItem)
                throw '[CartContext] Cannot to increase amount of cart item, because the cart does not have an item with the specified ID'
            if (cartItem.product.availableAmount <= 0)
                throw '[CartContext] Cannot increase the amount of cart item, because the available amount is 0 (or even less)'

            cartItem.amount++
            newCartState.total += cartItem.product.price
        } else if (actionType === 'REMOVE_PRODUCT_FROM_CART') {
            delete newCartState.items[payload.productId]
        } else if (actionType === 'SET_ITEM_AMOUNT') {
            const cartItem = newCartState.items[payload.productId]
            if (!cartItem)
                throw '[CartContext] Tried to set amount of cart item, but the cart does not have an item with the specified ID'
            if (cartItem.product.availableAmount < payload.amount)
                throw '[CartContext] Cannot set cart item amount, because the available amount is exceeded'
            if (cartItem.product.minOrderAmount > payload.amount)
                throw '[CartContext] Cannot set cart item amount, because the minimum order amount would be violated'

            const difference = payload.amount - cartItem.amount
            cartItem.amount = payload.amount
            newCartState.total += difference * cartItem.product.price

            if (payload.amount === 0)
                delete newCartState.items[payload.productId]
        } else if (actionType === 'DECREASE_ITEM_AMOUNT') {
            const cartItem = newCartState.items[payload.productId]
            if (!cartItem)
                throw '[CartContext] Tried to decrease amount of cart item, but the cart does not have an item with the specified ID'
            if (cartItem.amount <= cartItem.product.minOrderAmount)
                throw '[CartContext] Cannot decrease the amount of cart item, because the minimum order amount is would be violated'

            if (cartItem.amount === 1)
                delete newCartState.items[payload.productId]
            else cartItem.amount--

            newCartState.total -= cartItem.product.price
        } else {
            // TODO Action: Empty the cart
            throw (
                '[CartContext] Wrong action, this should never happen - ' +
                '"' +
                actionType +
                '"'
            )
        }

        localStorage.setItem(`cart+guest`, JSON.stringify(newCartState))
    } catch (e) {
        console.log(e)
    }
    return newCartState
}
