import type { Meta, StoryObj } from '@storybook/react'
import { ProductDisplay } from '../components'
import { CURRENCIES, ORDER_UNITS, Product } from '../model/model'

const meta = {
    title: 'ProductDisplay',
    component: ProductDisplay,
    parameters: {
        layout: 'centered'
    },
    tags: ['autodocs'],
    argTypes: {},
    args: {}
} satisfies Meta<typeof ProductDisplay>

export default meta
type Story = StoryObj<typeof meta>

const product: Product = {
    name: 'Product',
    availableAmount: 11,
    id: 'product',
    img: 'src/assets/visuals/icons/app/app-icon-512x512.svg',
    minOrderAmount: 3,
    price: 7,
    currency: CURRENCIES.us_dollar,
    unit: ORDER_UNITS.piece
} as const

const args: Story['args'] = {
    cartItem: {
        product,
        amount: 5
    },
    product,
    renderControls: (
        availableInMinOrderAmount: boolean,
        remainingAvailableAmount: number
    ) => (
        <>
            <div>
                {availableInMinOrderAmount ? 'Available' : 'Out of stock'}
            </div>
            <div>Remaining: {remainingAvailableAmount}</div>
        </>
    ),
    actionsElement: <>Actions</>
} as const

type StoryDecorator = Story['decorators']
const ulDecorator: StoryDecorator = (Story) => (
    <ul>
        <Story />
    </ul>
)
const ulDecoratorDark: StoryDecorator = (Story) => (
    <ul className="dark">
        <Story />
    </ul>
)

const BaseStory: Story = {
    args,
    decorators: ulDecorator
}

export const Render: Story = {
    ...BaseStory
}
export const Dark: Story = {
    ...BaseStory,
    decorators: ulDecoratorDark
}

const productMin1: Product = {
    ...product,
    minOrderAmount: 1
}

export const NoMinOrderAmount: Story = {
    ...BaseStory,
    args: {
        ...args,
        product: productMin1
    },

    decorators: ulDecorator
}

const productOutOfStock: Product = {
    ...product,
    availableAmount: 0
}

export const OutOfStock: Story = {
    ...BaseStory,
    args: {
        ...args,
        product: productOutOfStock
    },
    decorators: ulDecorator
}
