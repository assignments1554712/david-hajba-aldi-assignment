import { CartItem, ORDER_UNITS, OrderUnit, Product } from '../model/model'

export function orderAmount(
    amount: number,
    unit: OrderUnit = ORDER_UNITS.piece
) {
    return `${amount} ${amount > 1 ? unit.abbrPlural : unit.abbr}`
}

/**
 * @param direction 1 for ascending, -1 for descending
 * @returns
 */
export const getSortingCartItemByPrice =
    (direction: SortingOrder) =>
    (a: CartItem, b: CartItem): SortingOrder =>
        (sortProductByPrice(a.product, b.product) * direction) as SortingOrder

/**
 * @param direction 1 for ascending, -1 for descending
 * @returns
 */
export const getSortingProductByPrice =
    (direction: SortingOrder) =>
    (a: Product, b: Product): SortingOrder =>
        (sortProductByPrice(a, b) * direction) as SortingOrder

export const sortProductByPrice = (a: Product, b: Product): SortingOrder =>
    a.price > b.price ? 1 : -1

export type SortingOrder = 1 | -1
