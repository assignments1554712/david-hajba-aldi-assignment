import { SortingOrder } from '../util/domain'

export type SortingButobProps = {
    sortingAttributeName: string
    sortingDirection: SortingOrder
    currentListLength: number
    onClick: () => void
}

const SortingButton: React.FC<SortingButobProps> = ({
    sortingAttributeName,
    sortingDirection,
    currentListLength,
    onClick
}) => (
    <button
        onClick={onClick}
        className="btn btn-transparent form-control-compact"
        disabled={!currentListLength}
        title={
            currentListLength
                ? `Sort by ${sortingAttributeName[0].toUpperCase()}${sortingAttributeName.substring(1).toLocaleLowerCase()}`
                : 'No Items to sort by Price'
        }
    >
        {sortingDirection - 1 ? '▼' : '▲'}
    </button>
)

export default SortingButton
