import React from 'react'
import appIcon from '../../../../assets/visuals/icons/app/app-icon-512x512.png'
import { Space } from '../../../../model/model'
import { classNames } from '../../../../util/browser/dom'
import styles from './AppIcon.module.scss'

type AppIconProps = {
    className?: string
    size?: Space
}

export const AppIcon: React.FC<AppIconProps> = ({
    className = '',
    size = 6,
    ...props
}) => (
    <div
        style={{ backgroundImage: `url(${appIcon})` }}
        className={classNames(
            `w-${size}`,
            'transition-all duration-600',
            className,
            styles.AppIcon
        )}
        {...props}
    ></div>
)
