import React from 'react'
import { ProductsListContextProvider } from '../../../store/ProductsList/ProductsListContext'
import { ProductsList } from '../ProductsList/ProductsList'

interface IProductsProps {}

export const Products: React.FC<IProductsProps> = () => {
    return (
        <ProductsListContextProvider>
            <ProductsList />
        </ProductsListContextProvider>
    )
}
