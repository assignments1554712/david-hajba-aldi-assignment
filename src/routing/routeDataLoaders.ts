/* eslint-disable @typescript-eslint/no-unused-vars */

import { LoaderFunctionArgs } from 'react-router-dom'
import { fetchProducts } from '../mock/products'
import { Product } from '../model/model'

export const loadProducts = async (args: LoaderFunctionArgs<Product[]>) => {
    try {
        // const products = await fetch('https://63c10327716562671870f959.mockapi.io/products')
        const products = (await fetchProducts()) as Product[]
        return products
    } catch (e) {
        console.log('[route loader loadProducts] Error loading Gallery data:', {
            e
        })
        throw e
    }
}
