import React, { useContext, useEffect, useRef, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { BUILD } from '../../../main'
import ThemeContext from '../../../store/Theme/ThemeContext'
import { AppIcon } from '../icon/AppIcon/AppIcon'
import './Menu.scss'

export const Menu: React.FC = () => {
    const [isOpen, setIsOpen] = useState(false)
    const menuRef = useRef<HTMLDivElement>(null)

    const { theme, toggleTheme } = useContext(ThemeContext)
    const navigate = useNavigate()

    useEffect(() => {
        const handleClickOutside = (event: MouseEvent) => {
            if (
                menuRef.current &&
                !menuRef.current?.contains(event.target as Node)
            ) {
                setIsOpen(false)
            }
        }

        if (isOpen) {
            // Attach the event listener when the component mounts
            setTimeout(() =>
                document.addEventListener('click', handleClickOutside)
            )
        } else {
            document.removeEventListener('click', handleClickOutside)
        }

        // Detach the event listener when the component unmounts
        return () => {
            document.removeEventListener('click', handleClickOutside)
        }
    }, [isOpen])

    const toggleMenu = () => {
        setIsOpen((x) => !x)
    }

    const userMenuFragment = (
        <ul className="">
            <li className="flex items-center gap-4 p-4">
                <AppIcon className="cursor-pointer" />
                <div>Menu</div>
            </li>
            <li className="app-menu border-y dark:border-shade-800 border-shade-250">
                {[
                    {
                        onClick: () => navigate('/products'),
                        text: '> Products'
                    },
                    {
                        onClick: () => navigate('/cart'),
                        text: '> Cart'
                    },
                    {
                        onClick: () => toggleTheme(),
                        text: `Switch to ${
                            theme === 'dark' ? 'Light' : 'Dark'
                        } Theme`
                    }
                ].map((menuItem, i) => (
                    <a
                        onClick={menuItem.onClick}
                        className="p-4 decoration-transparent-200"
                        key={i}
                    >
                        {menuItem.text}
                    </a>
                ))}
            </li>
            <li className="px-4 py-2">
                <small className="flex items-center text-secondary">
                    Build:<pre> {BUILD}</pre>
                </small>
            </li>
        </ul>
    )

    return (
        <>
            <div className="menu relative" onClick={toggleMenu}>
                <AppIcon className="cursor-pointer" />
                {isOpen && (
                    <div
                        ref={menuRef}
                        className="absolute -top-4 -left-4 dark:bg-shade-950 bg-shade-100"
                    >
                        {userMenuFragment}
                    </div>
                )}
            </div>
        </>
    )
}
