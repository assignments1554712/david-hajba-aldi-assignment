import React from 'react'
import { CartItems } from './CartItems/CartItems'

interface IProductsProps {}

export const Cart: React.FC<IProductsProps> = () => {
    return <CartItems />
}
