import React from 'react'
import { Outlet } from 'react-router-dom'
import { CartContextProvider } from '../../../store/Cart/CartContext'
import Header from './Header'

export const RootLayout: React.FC = () => {
    return (
        <>
            <div className="max-w-4xl mx-auto my-0 bg-view px-2">
                <CartContextProvider>
                    <header>
                        <Header />
                    </header>
                    <main className="max-w-4xl mx-auto my-0 bg-view">
                        <Outlet />
                    </main>
                </CartContextProvider>
            </div>
            <footer className="max-w-6xl mx-auto my-0 dark:bg-shade-950 bg-shade-100"></footer>
        </>
    )
}
