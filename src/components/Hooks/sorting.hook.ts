import { useState } from 'react'
import { SortingOrder } from '../../util/domain'

export function useSorting<T>(
    sortOrder: (order: SortingOrder) => (a: T, b: T) => SortingOrder
) {
    const [sortingDirection, setSortingDirection] = useState<SortingOrder>(1)
    const toggleSortingDirection = () =>
        setSortingDirection((x) => -x as SortingOrder)
    const sorting = sortOrder(sortingDirection)

    return { sorting, sortingDirection, toggleSortingDirection }
}
