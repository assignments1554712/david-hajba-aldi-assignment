import { signal } from '@preact/signals-react'

const searchSignal = signal('')

export default searchSignal
