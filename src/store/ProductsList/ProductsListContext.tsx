import {
    ReactNode,
    createContext,
    useEffect,
    useReducer,
    useState
} from 'react'
import SnackBar from '../../components/ui/elements/SnackBar/SnackBar'
import { Product, RequestError } from '../../model/model'
import { deepFreeze } from '../../util/object'

type ProductsListActon =
    | {
          type: 'SET_PRODUCTS_LIST'
          payload: Product[]
      }
    | {
          type: 'SET_AVAILABLE_AMOUNT'
          payload: { productId: string; availableAmount: number }
      }

const ProductsListContext = createContext<{
    productsList: Product[]
    setProductsList: (productsList: Product[]) => void
    setAvailableAmount: (productId: string, availableAmount: number) => void
}>({
    productsList: [],
    setProductsList: (productsList: Product[]) =>
        console.warn(
            '[ProductsListContext.tsx] Invoked the initial action for Context ProductsListCtxt > setProductsList(productsList: Product[])',
            productsList
        ),
    setAvailableAmount: (productId: string, availableAmount: number) =>
        console.warn(
            '[ProductsListContext.tsx] Invoked the initial action for Context ProductsListCtxt > setAvailableAmount(productId: string, availableAmount: number)',
            productId,
            availableAmount
        )
})

const ProductsListContextProvider = ({ children }: { children: ReactNode }) => {
    const [productsListState, productsListDispatch] = useReducer<
        (state: Product[], action: ProductsListActon) => Product[]
    >(productsListReducer, [])

    const [error, setError] = useState<RequestError | null>(null)

    function handleSetProductsList(productsList: Product[]) {
        productsListDispatch({
            type: 'SET_PRODUCTS_LIST',
            payload: productsList
        })
    }
    function handleSetAvailableAmount(
        productId: string,
        availableAmount: number
    ) {
        productsListDispatch({
            type: 'SET_AVAILABLE_AMOUNT',
            payload: { productId, availableAmount }
        })
    }
    console.log('[ProductsListContext] ...render...')
    useEffect(() => {
        (async () => {
            try {
                fetch('https://63c10327716562671870f959.mockapi.io/products')
                    .then((response) => response.json())
                    .then((products) => handleSetProductsList(products))
            } catch (e) {
                setError({ e, message: 'Could not load the Products' })
            }
        })()
    }, [])

    return (
        <ProductsListContext.Provider
            value={{
                productsList: productsListState,
                setProductsList: handleSetProductsList,
                setAvailableAmount: handleSetAvailableAmount
            }}
        >
            {children}
            <SnackBar
                message={error?.message}
                level="error"
                onDismissed={() => setError(null)}
            />
        </ProductsListContext.Provider>
    )
}

export default ProductsListContext
export { ProductsListContextProvider }

function productsListReducer(
    state: Product[],
    { type: actionType, payload }: ProductsListActon
): Product[] {
    let newProductsListState: Product[] = structuredClone(state)

    if (actionType === 'SET_PRODUCTS_LIST') {
        newProductsListState = payload
        deepFreeze(newProductsListState)
    } else if (actionType === 'SET_AVAILABLE_AMOUNT') {
        newProductsListState.find(
            (p) => (p.availableAmount = payload.availableAmount)
        )
    } else {
        throw (
            '[ProductsListContext] Wrong action, this should never happen - ' +
            '"' +
            actionType +
            '"'
        )
    }

    return newProductsListState
}
