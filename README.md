# Aldi test assignment

Dávid Hajba

This is a React project built with Vite

Template: react-ts

## System Requirements and setup

### Requirements

Vite requires Node.js version 18+. 20+

This application does not support screens narrower than 340px in width.

### Setup

1. Open a terminal or cmd
2. Go to the project root directory
3. Issue

```bash
    npm i
```

### Run the development server

```bash
    npm run dev
```

### Execute Unit tests

```bash
    npm test
```

#### Notes

Find unit test samples:

-   `CartContext.test.tsx` - component test
-   `object.test.ts` - plain module test

### Run Storybook

Run Storybook and browse the stories.

The Storybook host is at http://localhost:6006/

```bash
    npm run storybook
```

#### Notes

The `storybook-tailwind-dark-mode` addon is not installed because of dependency resolution issues, so the dark appearance is faulty.

-   TODO fix dependencies

### Execute Storybook tests

1. Run Storybook
2. In a separate terminal, issue

```bash
    npm run test-storybook
```

#### Notes

Find sample assertions in the following stories in `AmountControl.stories.tsx`

## PWA

This is an installable PWA.

Build the project and deploy the `dist` directory

I didn't include a test deployment server, please use one of your choice.

```bash
    npm run build
```
