import React from 'react'
import imgAlt from '../../../../assets/visuals/img-alt.svg'
import productImgAlt from '../../../../assets/visuals/product-img-alt.svg'
import { classNames } from '../../../../util/browser/dom'

type ImageProps = {
    onClick?: () => void
    imgSrc?: string
    thumbnail?: boolean
    product?: boolean
    className?: string
}

const Image: React.FC<ImageProps> = ({
    onClick,
    imgSrc,
    thumbnail = true,
    product = false,
    className = ''
}) => {
    return (
        <div
            style={{
                backgroundImage: `url(${imgSrc || (product ? productImgAlt : imgAlt)}), url(${product ? productImgAlt : imgAlt})`,
                backgroundPosition: 'center',
                backgroundSize: 'contain',
                backgroundRepeat: 'no-repeat'
            }}
            className={classNames(
                product ? 'bg-shade-0' : 'dark:bg-shade-850 bg-shade-200',
                'flex items-center justify-center aspect-square',
                thumbnail ? ' xs:h-44' : 'h-44',
                className
            )}
            onClick={onClick}
        ></div>
    )
}

export default Image
