/* eslint-disable @typescript-eslint/no-unused-vars */

import { expect } from '@storybook/jest'
import type { Meta, StoryObj } from '@storybook/react'
import { fn, userEvent } from '@storybook/test'
import { waitFor, within } from '@storybook/testing-library'
import AmountControl from '../components/AmountControl'
import { CURRENCIES, ORDER_UNITS, Product } from '../model/model'

const meta = {
    title: 'AmountControl',
    component: AmountControl,
    parameters: {
        layout: 'centered'
    },
    tags: ['autodocs'],
    argTypes: {},
    args: {
        onSubmit: fn()
    }
} satisfies Meta<typeof AmountControl>

export default meta

type Story = StoryObj<typeof meta>

const availableAmount = 11
const minOrderAmount = 3
const product: Product = {
    name: 'Product',
    availableAmount,
    id: 'product',
    img: 'src/assets/visuals/icons/app/app-icon-512x512.svg',
    minOrderAmount,
    price: 7,
    currency: CURRENCIES.us_dollar,
    unit: ORDER_UNITS.piece
} as const

const args: Story['args'] = {
    product,
    submitText: 'Submit',
    availableInMinOrderAmount: true,
    remainingAvailableAmount: 11,
    onSubmit: fn()
} as const

type StoryDecorator = Story['decorators']
const ulDecorator: StoryDecorator = (Story) => (
    <div>
        <div className="p-4 bg-card">
            <Story />
        </div>
    </div>
)
const ulDecoratorDark: StoryDecorator = (Story) => (
    <div className="dark">
        <div className="p-4 bg-card">
            <Story />
        </div>
    </div>
)

const BaseStory: Story = {
    args,
    decorators: ulDecorator
}

export const Render: Story = {
    ...BaseStory,
    play: async ({ args, canvasElement, step }) => {
        const canvas = within(canvasElement)

        await waitFor(() => {
            expect(
                canvas.getByTestId('amount-control-submit')
            ).not.toBeDisabled()
            expect(canvas.getByTestId('amount-control-decrease')).toBeDisabled()
        })
    }
}

export const Dark: Story = {
    ...BaseStory,
    decorators: ulDecoratorDark
}

export const AmountChanged: Story = {
    ...BaseStory,
    args: {
        ...args,
        cartItemAmount: 5
    },
    play: async ({ args, canvasElement, step }) => {
        const canvas = within(canvasElement)

        await step('Submit Amount', async () => {
            await userEvent.click(canvas.getByTestId('amount-control-submit'))
        })

        await waitFor(() => {
            expect(args.onSubmit).toHaveBeenCalled()
        })
    }
}

export const AmountUnchanged: Story = {
    ...BaseStory,
    args: {
        ...args,
        cartItemAmount: 3
    },
    play: async ({ args, canvasElement, step }) => {
        const canvas = within(canvasElement)

        await waitFor(() => {
            expect(canvas.getByTestId('amount-control-submit')).toBeDisabled()
        })
    }
}

export const AvailableAmountReached: Story = {
    ...BaseStory,
    args: {
        ...args,
        remainingAvailableAmount: 0
    },
    play: async ({ args, canvasElement, step }) => {
        const canvas = within(canvasElement)

        await waitFor(() => {
            expect(canvas.getByTestId('amount-control-increase')).toBeDisabled()
        })
    }
}
