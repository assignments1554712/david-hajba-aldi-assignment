if (!global.structuredClone) {
    global.structuredClone = structuredClonePolyfill
}

function structuredClonePolyfill(obj) {
    return JSON.parse(JSON.stringify(obj))
}
